﻿  using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Lavalink;
  using DSharpPlus.Lavalink.EventArgs;

  namespace DjCinderela.Commands;

public class BasicCommands : BaseCommandModule
{
    
    private List<LavalinkTrack> _queue;
    private LavalinkTrack _currentTrack;
    private CommandContext _ctx;
    private readonly LavalinkNodeConnection _lavaNode;
    
    [Command("ping")]
    [Description("Ping")]
    public async Task PingAsync(CommandContext ctx)
    {   
        await ctx.TriggerTypingAsync();

        await ctx.RespondAsync("pong");
        await Task.Delay(2000);
        await ctx.TriggerTypingAsync();
        await ctx.RespondAsync($"Brincadeira kkkkkkkkkkk  ping é {ctx.Client.Ping.ToString() +"ms"}");
    }
    
    [Command("entrar")]
    [Description("Join chanel")]
    public async Task JoinAsync(CommandContext ctx,DiscordChannel channel  = null)
    {

        if (channel == null)
        {
            channel = ctx.Member.VoiceState.Channel;
        }
        
        var lava = ctx.Client.GetLavalink();
        if (!lava.ConnectedNodes.Any())
        {
            await ctx.RespondAsync("lavalink não estabeleceu conexão");
            return;
        }

        var node = lava.ConnectedNodes.Values.First();

        if (channel.Type != ChannelType.Voice)
        {
            await ctx.RespondAsync("Canal invalido.");
            return;
        }
        if (node.IsConnected == false)
        {
            await ctx.RespondAsync($"Entrou no canal {channel.Name}!");
        }
        
        await node.ConnectAsync(channel);
    }
    
    [Command("tocar")]
    public async Task Play(CommandContext ctx, [RemainingText] string search)
    {
        await JoinAsync(ctx,ctx.Member.VoiceState.Channel);

        _ctx = ctx;
        
        if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
        {
            await ctx.RespondAsync("Voce nao esta em nenhum canal.");
            return;
        }

        var lava = ctx.Client.GetLavalink();
        var node = lava.ConnectedNodes.Values.First();
        var conn = node.GetGuildConnection(ctx.Member.VoiceState.Guild);
        
        if (conn == null)
        {
            await ctx.RespondAsync("Lavalink não esta conectado.");
            return;
        }
        
        var loadResult = await node.Rest.GetTracksAsync(search);                  
                                                                          
        if (loadResult.LoadResultType == LavalinkLoadResultType.LoadFailed        
            || loadResult.LoadResultType == LavalinkLoadResultType.NoMatches)     
        {                                                                         
            await ctx.RespondAsync($"Não consegui pesquisar por {search}.");      
            return;                                                               
        }                                                                         
                                                                          
        var track = loadResult.Tracks.First();

        node.PlaybackFinished += trackFinished;

        if (_queue == null)
        {
            _queue = new List<LavalinkTrack>();
        }
        
        if (_currentTrack == null)
        {
            _currentTrack = track;
            await conn.PlayAsync(track);
            await ctx.RespondAsync($"Tocando agora {track.Title}!");
        }
        else
        {
            _queue.Add(track);
           await ctx.RespondAsync($"Música adicionada na posição {_queue.Count} da fila.");  
        }
    }
    
    private async Task trackFinished(LavalinkGuildConnection sender, TrackFinishEventArgs args) {
        if (_queue.Count > 0) {
            _currentTrack = _queue[0];
            _queue.RemoveAt(0);
            await sender.PlayAsync(_currentTrack);
            await JoinAsync(_ctx,_ctx.Member.VoiceState.Channel);
        } else {
            _currentTrack = null;
        }
    }

    [Command("parar")]
    public async Task Stop(CommandContext ctx)
    {
        Pause(ctx);
    }
    
    [Command("pausar")]
    public async Task Pause(CommandContext ctx)
    {
        if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
        {
            await ctx.RespondAsync("Voce nao esta em nenhum canal.");
            return;
        }

        var lava = ctx.Client.GetLavalink();
        var node = lava.ConnectedNodes.Values.First();
        var conn = node.GetGuildConnection(ctx.Member.VoiceState.Guild);

        if (conn == null)
        {
            await ctx.RespondAsync("Lavalink não esta conectado.");
            return;
        }
        
        await conn.PauseAsync();

        await ctx.RespondAsync($"Musica pausada!");
    }

    [Command("volume")]
    public async Task SetVolume(CommandContext ctx, int value)
    {
        if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
        {
            await ctx.RespondAsync("Voce nao esta em nenhum canal.");
            return;
        }

        var lava = ctx.Client.GetLavalink();
        var node = lava.ConnectedNodes.Values.First();
        var conn = node.GetGuildConnection(ctx.Member.VoiceState.Guild);

        if (conn == null)
        {
            await ctx.RespondAsync("Lavalink não esta conectado.");
            return;
        }
        
        await conn.SetVolumeAsync(value);

        await ctx.RespondAsync($"Pronto!");
    }
    
    [Command("despausar")]
    public async Task Despausar(CommandContext ctx)
    {
        if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
        {
            await ctx.RespondAsync("Voce nao esta em nenhum canal.");
            return;
        }

        var lava = ctx.Client.GetLavalink();
        var node = lava.ConnectedNodes.Values.First();
        var conn = node.GetGuildConnection(ctx.Member.VoiceState.Guild);

        if (conn == null)
        {
            await ctx.RespondAsync("Lavalink não esta conectado.");
            return;
        }
        
        await conn.ResumeAsync();

        await ctx.RespondAsync($"Musica Despausada!");
    }
    
    [Command("sair")]
    public async Task Leave(CommandContext ctx, DiscordChannel channel = null)
    {
        if (channel == null)
        {
            channel = ctx.Member.VoiceState.Channel;
        }
        
        var lava = ctx.Client.GetLavalink();
        if (!lava.ConnectedNodes.Any())
        {
            await ctx.RespondAsync("A conexão com o navlink nao esta estabelecida");
            return;
        }

        var node = lava.ConnectedNodes.Values.First();

        if (channel.Type != ChannelType.Voice)
        {
            await ctx.RespondAsync("Nenhum canal de voz valido.");
            return;
        }

        var conn = node.GetGuildConnection(channel.Guild);

        if (conn == null)
        {
            await ctx.RespondAsync("Lavalink não esta conectado.");
            return;
        }
        await conn.DisconnectAsync();
    }

    [Command("manda")]
    public async Task Message(CommandContext ctx,String message)
    {
        var DMChannel = await ctx.Member.CreateDmChannelAsync();
        await DMChannel.SendMessageAsync(message);
    }
}