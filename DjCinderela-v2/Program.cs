﻿using DjCinderela.Commands;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Lavalink;
using DSharpPlus.Net;
using Microsoft.Extensions.Logging;

namespace DjCinderela
{
    class Program
    {
        private DiscordClient _client;

        static void Main(String[] args) => new Program().RobotAsync().GetAwaiter().GetResult();

        public async Task RobotAsync()
        {
            DiscordConfiguration cfg = new DiscordConfiguration
            {
                Token = "MTA4NDE3NzkyMDU0Njg1Mjg3NQ.GZY8Id.F98uOzCWavAQu29WYGT3WRZaq4KjmUvuGrE7Uc",
                TokenType = TokenType.Bot,
                GatewayCompressionLevel = GatewayCompressionLevel.Stream,
                AutoReconnect = true,
                MinimumLogLevel = LogLevel.Debug,
                UseRelativeRatelimit = true,
                Intents = DiscordIntents.All,
            };

            _client = new DiscordClient(cfg);

            _client.Ready += Client_Ready;
            _client.ClientErrored += Client_Error;
            _client.MessageCreated += Cnt_MessageCreated;
            

            CommandsNextExtension cnt = _client.UseCommandsNext(new CommandsNextConfiguration()
            {
                StringPrefixes = new[] { "$" },
                EnableDms = false,
                CaseSensitive = false,
                EnableDefaultHelp = false,
                EnableMentionPrefix = true,
                IgnoreExtraArguments = true,
            });
            
           cnt.CommandExecuted += Cnt_CommandExecuted;
           cnt.RegisterCommands<BasicCommands>();
           
           await _client.ConnectAsync();
            await Task.Delay(-1);
        }

        private async Task Client_Ready(DiscordClient _client,ReadyEventArgs e)
        {
            await ConnectyLavalink();

            await StartServerAndUpdate();
        }

        private  Task Cnt_CommandExecuted(CommandsNextExtension command, CommandExecutionEventArgs e)
        {

            Console.WriteLine("alguma coisa foi digitada comando");
            return Task.CompletedTask;
        }

        private async Task Cnt_MessageCreated(DiscordClient client, MessageCreateEventArgs message)
        {

            Console.WriteLine(message.Message.Content);
            if (message.Message.Content.StartsWith("!ping"))
            await message.Message.RespondAsync("Pong!");
        }

       private async Task StartServerAndUpdate()
        {
            DiscordChannel channel = await _client.GetChannelAsync(1084201841585291346);

            DiscordEmbedBuilder embed = new DiscordEmbedBuilder
            {
                Color = DiscordColor.SpringGreen,
                Description = "Cheguei!",
                Title = "Irra"
            };
            
            await channel.SendMessageAsync(embed: embed);

            await _client.UpdateStatusAsync(new DiscordActivity("Voce",DSharpPlus.Entities.ActivityType.ListeningTo),DSharpPlus.Entities.UserStatus.DoNotDisturb);
        }
        
        private async Task ConnectyLavalink()
        {
            var lavalinkConfig = new LavalinkConfiguration
            {
                Password = "youshallnotpass",
                SocketEndpoint = new ConnectionEndpoint()
                {
                    Hostname = "localhost",
                    Port = 2333
                },
                RestEndpoint = new ConnectionEndpoint
                {
                    Hostname = "localhost",
                    Port = 2333
                }
            };

            var _lavalinkNodeConnection =  _client.UseLavalink();
            await _lavalinkNodeConnection.ConnectAsync(lavalinkConfig);
        }

        private  Task Client_Error(DiscordClient _client,ClientErrorEventArgs e)
        {
            Console.WriteLine(e.ToString());

            return Task.CompletedTask;
        }
    }
}